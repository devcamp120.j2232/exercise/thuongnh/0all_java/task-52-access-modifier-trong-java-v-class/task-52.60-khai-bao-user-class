package models;

public class User {
    private String Password;
    private String Username;
    private String Enabled;
    public User() {
    }
    public User(String password, String username, String enabled) {
        Password = password;
        Username = username;
        Enabled = enabled;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String password) {
        Password = password;
    }
    public String getUsername() {
        return Username;
    }
    public void setUsername(String username) {
        Username = username;
    }
    public String getEnabled() {
        return Enabled;
    }
    public void setEnabled(String enabled) {
        Enabled = enabled;
    }
    @Override
    public String toString() {
        return "User [Password=" + Password + ", Username=" + Username + ", Enabled=" + Enabled + "]";
    }

    
    
    
    

}
