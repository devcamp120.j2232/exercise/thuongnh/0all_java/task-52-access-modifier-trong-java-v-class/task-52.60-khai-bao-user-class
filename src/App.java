import  models.*;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // khởi tạo hai đối tượng 
        User user1 = new User();
        User user2 = new User("thuongm4", "@thhuongnh0404", "true");


        // in hai đối tượng trên ra màn hình 
        System.out.println("chưa thay đổi thuộc tính của đối tượng qua phương thức set");
        System.out.println(user1);
        System.out.println(user2);

        // thay đổi thuộc tính của đối tượng 1;
        user1.setUsername("linh");
        user1.setPassword("linh1234");
        user1.setEnabled("false");

        // in đối tượng 1 qua lần 1 thay đổi 
        System.out.println("in đối tượng 1 qua lần 1 thay đổi ");
        System.out.println(user1);

    }
}
